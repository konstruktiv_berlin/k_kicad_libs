* Kurzbeschreibung:
* Betrag (brutto):

* [ ] bezahlt per 
* [ ] Rechnung liegt vor (ggf. label ~"wartet auf Rechnung" entfernen)
* [ ] geliefert (ggf. label ~"wartet auf Lieferung" entfernen)
* [ ] in Buchhaltung
* [ ] netto Betrag in Kimai (Bestellungen f�r B�ro oder Werkstatt bitte im Projekt "intern" buchen)

Infos zum Workflow im [Konstruktiv Buchhaltung Wiki](https://konstru.de/i/gitlab/Konstruktiv/Buchhaltung/wikis/Workflow-Bestellungen):

/label ~Bestellung
/due in one week
/assign @
